var express = require('express');
var router = express.Router();
const mysql = require('mysql')
const con = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'sitepoint'
});

var connectDB = () => {
    con.connect((err) => {
      if (err) {
        console.log("Error connecting to DB");
        return;
      }
      console.log("Connecttion establishe");
    })
    return con;
  }
  /* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});
router.post('/add', (req, res, next) => {
  const author = req.body
  var lastId = con.query('INSERT INTO authors SET ?',
    author, (err, response) => {
      if (err) throw err
      console.log(response.insertId, author.name)
      res.render('show.html', { id: response.insertId });
    });

});
module.exports = router;