var express = require('express');
var router = express.Router();
const mysql = require('mysql')


const con = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'person'
});

var connectDB = () => {
    con.connect((err) => {
        if (err) {
            console.log("Error connecting to DB");
            return;
        }
        console.log("Connection establishe");
    })
    return con;
}


/* GET home page. http://127.0.0.1:3000/person/login */

router.get('/login', function (req, res, next) {
    let connect = connectDB();
    res.render("./person/login.html")
})


router.post('/login', function (req, res, next) {
    let user = req.body
    console.log(user)
    let query = con.query('SELECT * FROM mst_person_information WHERE email = ? AND password = ? LIMIT 1', [user.email, user.password], (err, row, next) => {
        if (err) throw err
        if (row[0] == null)
            res.render('./person/nomember.html')
        else
            res.render('./person/member.html', { data: row[0] })
    })

})


module.exports = router;