const express = require('express');
const app = express();
const port = 3000;
const mysql = require('mysql');
const bodyParser = require('body-parser')

app.set('view engine','pug');

app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

var connection = mysql.createConnection({
	host : 'localhost',
	user : 'root',
	password : '',
	database : 'demo'
});

connection.connect(function(err) {
  if (err) throw err;
  console.log("Connected!");
});

//app.get('/',(req,res) => res.send('hello world'));
var usestatus = ""
app.get('/show',(req,res) => {
	connection.query('SELECT * FROM posts',(err,result) => {
		res.render('index',{
			posts:result,
			usestatus:usestatus
		});
		console.log(usestatus)
	})
});

app.get('/add',(req,res) => {
	res.render('add');
});

app.post('/add',(req,res) => {
	const title = req.body.title;
	const content = req.body.content;
	const author_name = req.body.author_name;
	const post = {
		title:title,
		content:content,
		author:author_name,
		created_at:new Date()
	}
	connection.query('INSERT INTO posts SET ?',post,(err) => {
		console.log('Data Inserted');
		console.log(usestatus)
		return res.redirect('/show');
	});
});

app.get('/edit/:id',(req,res) => {
	
	const edit_postID = req.params.id;
	
	connection.query('SELECT * FROM posts WHERE id=?',[edit_postID],(err,results) => {
		if(results){
			res.render('edit',{
				post:results[0]
			});
		}
	});
});

app.post('/edit/:id',(req,res) => {
	const update_title = req.body.title;
	const update_content = req.body.content;
	const update_author_name = req.body.author_name;
	const userId = req.params.id;
	connection.query('UPDATE `posts` SET title = ?, content = ?, author = ? WHERE id = ?', [update_title, update_content, update_author_name, userId], (err, results) => {
        if(results.changedRows === 1){
            console.log('Post Updated');
        }
		return res.redirect('/show');
    });
});

app.get('/delete/:id',(req,res) => {
    connection.query('DELETE FROM `posts` WHERE id = ?', [req.params.id], (err, results) => {
		return res.redirect('/show');
    });	
});
app.get('/person', (req, res) => {
	connection.query('SELECT * FROM MST_Person_Information', (err, result) => {
		res.render('person.pug', {
			people: result
		});
	})
});

app.get('/person/add', (req, res) => {
	res.render('addPerson.pug');
});

app.post('/person/add', (req, res) => {
	const name = req.body.name;
	const age = req.body.age;
	const movie = req.body.movie;
	const email = req.body.email;
	const password = req.body.password;
	const status = req.body.status;
	const person = {
		name: name,
		age: age,
		movie: movie,
		email: email,
		password: password,
		status: status
	}
	connection.query('INSERT INTO MST_Person_Information SET ?', person, (err) => {
		console.log('Data Inserted');
		return res.redirect('/person');
	});
});

app.get('/person/edit/:id', (req, res) => {

	const edit_postID = req.params.id;

	connection.query('SELECT * FROM MST_Person_Information WHERE id=?', [edit_postID], (err, results) => {
		if (results) {
			res.render('editPerson.pug', {
				person: results[0]
			});
		}
	});
});

app.post('/person/edit/:id', (req, res) => {
	const name = req.body.name;
	const age = req.body.age;
	const movie = req.body.movie;
	const email = req.body.email;
	const password = req.body.password;
	const userId = req.params.id;
	const status = req.body.status
	connection.query('UPDATE `MST_Person_Information` SET name = ?, age = ?, movie = ?,email = ?, password = ? WHERE id = ?', [name, age, movie, email, password, userId, status], (err, results) => {
		if (results.changedRows === 1) {
			console.log('Post Updated');
		}
		return res.redirect('/person');
	});
});

app.get('/person/delete/:id', (req, res) => {
	connection.query('DELETE FROM `MST_Person_Information` WHERE id = ?', [req.params.id], (err, results) => {
		return res.redirect('/person');
	});
});

app.get('/',(req,res) => {
	res.render('login');
})
app.post("/", function (req, res, next) {
	var data = {
		email: req.body.email,
		pass: req.body.password
	};
	console.log(data);
	var sql = "SELECT * FROM MST_Person_Information WHERE email = ? AND password = ?";
	connection.query(sql, [data.email, data.pass], function (err, result) {
		if (result.length > 0) {
			connection.query('SELECT * FROM posts',(err,result2) => {
				
			res.render('index.pug', {
				person:result[0],
				posts:result2,
				usestatus:result[0].status
			});
			})
			console.log(result)
			usestatus=result[0].status
			// console.log(usestatus)
		} else {
			res.render("notfound.pug", { title: "Express" });
		}
	});
});
app.listen(port, () => console.log(`Example app listening on port ${port}!`))